package com.ps.igotthisbook.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.ps.igotthisbook.R
import com.ps.igotthisbook.databinding.ActivitySplashScreenBinding
import android.content.Intent
import androidx.core.os.HandlerCompat.postDelayed


class SplashScreenActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashScreenBinding
    lateinit var handler: Handler
    lateinit var runnable: Runnable
    var delay_time: Long = 0
    var time = 3000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen)

        handler = Handler()

        runnable = Runnable {
            val intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

    }

    public override fun onResume() {
        super.onResume()
        delay_time = time
        handler.postDelayed(runnable, delay_time)
        time = System.currentTimeMillis()
    }

    public override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
        time = delay_time - (System.currentTimeMillis() - time)
    }
}
