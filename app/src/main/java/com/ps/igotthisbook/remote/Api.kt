package com.ps.igotthisbook.remote

import android.database.Observable
import com.ps.igotthisbook.model.Book
import retrofit2.Call
import retrofit2.http.GET


interface Api {

    @GET("/books.json")
    fun getData(): Call<List<Book>>

//    @GET("/books.json")
//    fun getData(): Observable<HashMap<String, Book>>

}