package com.ps.igotthisbook.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.ps.igotthisbook.model.Book
import com.ps.igotthisbook.remote.Api
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BasicRepositoryImpl(val remoteSource: Api) : BasicRepository {

    override fun getBookData(): LiveData<List<Book>> {
        val result = MediatorLiveData<List<Book>>()
        remoteSource.getData().enqueue(object : Callback<List<Book>> {
            override fun onFailure(call: Call<List<Book>>, t: Throwable) {
                result.value = null
            }

            override fun onResponse(call: Call<List<Book>>, response: Response<List<Book>>) {
                if (response.isSuccessful) {
                    result.value = response.body()
                }
            }
        })
        return result
    }

}