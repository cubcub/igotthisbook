package com.ps.igotthisbook.repository

import androidx.lifecycle.LiveData
import com.ps.igotthisbook.model.Book

interface BasicRepository {
    fun getBookData(): LiveData<List<Book>>
}